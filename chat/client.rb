require 'socket'

class Client
  def initialize(server)
    @server   = server
    listen
    send
    @request.join
    @response.join
  end

  def listen
    @response = Thread.new do
      loop {
        msg = @server.gets.chomp
        puts "#{msg}"
      }
    end
  end

  def send
    puts "Enter your username:"
    @request = Thread.new do
      loop {
        msg = $stdin.gets.chomp
        @server.puts(msg)
      }
    end
  end
end

server = TCPSocket.open '127.0.0.1', 3006
Client.new server

