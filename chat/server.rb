require 'socket'

class Server
  def initialize(ip, port)
    @server      = TCPServer.open ip, port
    @connections = Hash.new
    @clients     = Hash.new
    @connections[:clients] = @clients
  end

  def run
    loop {
      Thread.start(@server.accept) do |client|
        username = client.gets.chomp.to_sym
        @connections[:clients].each do |other_name, other_client|
          if username == other_name || client == other_client
            client.puts 'This username already exist'
            Thread.kill self
          end
        end

        puts "#{username} #{client}"
        @connections[:clients][username] = client
        client.puts 'Welcome!'

        listen_usr_msg(username, client)
      end
    }
  end

  def listen_usr_msg(username, client)
    loop {
      msg = client.gets.chomp
      # broadcast le message aux autres utilisateurs
      @connections[:clients].each do |other_name, other_client|
        unless other_name == username
          other_client.puts "#{username.to_s}: #{msg}"
        end
      end
    }
  end
end

server = Server.new '127.0.0.1', 3006
server.run

