require 'socket'
include Socket::Constants

# INET=>IP, STREAM=>TCP
socket = Socket.new :INET, :STREAM, 0
socket.bind (Socket.pack_sockaddr_in 3001, '127.0.0.1')

# 42=>backlog, nombre max de connections en attentes
socket.listen(42)

# affiche les infos de la connexion entrante
client_sock, client_addr = socket.accept
puts "client_sock: #{client_sock.inspect},\nclient_addr: #{client_addr.inspect}"

client_sock.puts 'press "q" to quit'

# boucle infinie
client_running = true
while client_running
  # affiche les 20 premiers caractères reçus
  data = client_sock.recvfrom(20)[0].chomp
  puts "data: #{data}\ndata length: #{data.length}"

  # renvoie hello world au client
  client_sock.puts 'Hello World! :)'

  # quitte si le client saisie "q"
  data == 'q' ? client_running = false : nil
end

client_sock.puts 'Quitting ...'
socket.close

