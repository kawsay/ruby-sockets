require 'socket'

srv = TCPServer.new '127.0.0.1', 3004

loop do
  # 1 Thread par connection
  Thread.start(srv.accept) do |client|
    # > IP:port
    puts "#{client.peeraddr[2]}:#{client.peeraddr[1]} est en ligne"

    loop do
      data = client.recvfrom(20)[0].chomp

      if data == 'q'
        client.puts 'Déconnexon...'
        puts "#{client.peeraddr[2]}:#{client.peeraddr[1]} s'est déconnecté"
        break	
      end

      puts "data: #{data}"
    end

    client.close
  end
end

