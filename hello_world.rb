require 'socket'
include Socket::Constants

# INET=>IP, STREAM=>TCP
socket = Socket.new :INET, :STREAM, 0
socket.bind (Socket.pack_sockaddr_in 3001, '127.0.0.1')

# 42=>backlog, nombre max de connections en attentes
socket.listen(42)

# affiche les infos de la connexion entrante
client_sock, client_addr = socket.accept
puts "client_sock: #{client_sock.inspect},\nclient_addr: #{client_addr.inspect}"

# affiche les 20 premiers caractères reçus
data = client_sock.recvfrom(20)[0].chomp
puts "data: #{data}"

# renvoie hello world au client
client_sock.puts "Hello World! :)"

# TIME_WAIT TCP state, attendre avant nouvelle connexion (EADDRINUSE)
socket.close

